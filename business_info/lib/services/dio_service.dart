import 'package:business_info/models/credential_model.dart';
import 'package:business_info/services/login_preferences.dart';
import 'package:dio/dio.dart';

class DioService {
  Dio _dio = Dio();

  Future createInterceptor() async {
    CredentialModel credentialModel =
        await LoginPreferences().readCredentials();

    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      options.headers["access-token"] = credentialModel.accessToken;
      options.headers["client"] = credentialModel.client;
      options.headers['uid'] = credentialModel.uid;

      return options;
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));

    return _dio;
  }
}
