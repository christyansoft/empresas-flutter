class ReponseData {
  bool success;
  var code;
  var message;
  var data;

  ReponseData({this.success = false, this.code, this.message, this.data});
}
