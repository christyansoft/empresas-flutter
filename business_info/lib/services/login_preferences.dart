import 'dart:convert';

import 'package:business_info/models/credential_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPreferences {
  saveCredentials(CredentialModel credentialModel) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('credential', json.encode(credentialModel.toJson()));
  }

  Future<CredentialModel> readCredentials() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var credentialJson = json.decode(prefs.getString('credential'));

    CredentialModel credentialModel = CredentialModel.fromJson(credentialJson);

    return credentialModel;
  }
}
