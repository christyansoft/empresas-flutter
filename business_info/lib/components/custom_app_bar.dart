import 'dart:io';

import 'package:flutter/material.dart';

customAppBar(title, context) {
  if (Platform.isAndroid)
    return AppBar(
      title: Text(title),
    );
  else
    return AppBar(
      title: Text(
        title,
        style: TextStyle(color: Colors.black),
      ),
      elevation: 0,
      automaticallyImplyLeading: false,
      leading: Container(
          // color: Colors.grey[300],
          child: BackButton(
        color: Theme.of(context).primaryColor,
      )),
      backgroundColor: Colors.white,
    );
}
