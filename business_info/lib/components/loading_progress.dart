import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

loadingProgress() => Platform.isAndroid
    ? Center(child: CircularProgressIndicator())
    : Center(child: CupertinoActivityIndicator());
