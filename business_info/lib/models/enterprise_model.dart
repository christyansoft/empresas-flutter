class EnterpriseModel {
  int id;
  String enterpriseName;
  String description;
  String emailEnterprise;
  bool ownEnterprise;
  String photo;
  int shares;
  double sharePrice;
  String city;
  String country;
  EnterpriseType enterpriseType;

  EnterpriseModel(
      {this.id,
      this.enterpriseName,
      this.description,
      this.emailEnterprise,
      this.ownEnterprise,
      this.photo,
      this.sharePrice,
      this.shares,
      this.city,
      this.country,
      this.enterpriseType});

  EnterpriseModel.fromJson(json) {
    print(json['shares']);
    id = json['id'];
    enterpriseName = json['enterprise_name'];
    description = json['description'];
    emailEnterprise = json['email_enterprise'];
    ownEnterprise = json['own_enterprise'];
    photo = json['photo'];
    shares = json['shares'];
    sharePrice = json['share_price'];
    city = json['city'];
    country = json['country'];
    enterpriseType = json['enterprise_type'] != null
        ? new EnterpriseType.fromJson(json['enterprise_type'])
        : null;
  }
}

class EnterpriseType {
  int id;
  String enterpriseTypeName;

  EnterpriseType({this.id, this.enterpriseTypeName});

  EnterpriseType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enterpriseTypeName = json['enterprise_type_name'];
  }
}
