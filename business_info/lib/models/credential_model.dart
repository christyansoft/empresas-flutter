class CredentialModel {
  String accessToken;
  String client;
  String uid;

  CredentialModel({this.accessToken, this.client, this.uid});

  CredentialModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    client = json['client'];
    uid = json['uid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    data['client'] = this.client;
    data['uid'] = this.uid;
    return data;
  }
}
