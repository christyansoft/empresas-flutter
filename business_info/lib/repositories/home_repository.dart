import 'package:business_info/models/enterprise_model.dart';
import 'package:business_info/services/config_api.dart';
import 'package:business_info/services/dio_service.dart';
import 'package:business_info/services/response_data.dart';
import 'package:dio/dio.dart';

class HomeRepository {
  Future<ReponseData> searchByName(String query) async {
    Dio dio = await DioService().createInterceptor();

    String url = '$baseUrl/enterprises?name=$query';

    try {
      Response response = await dio.get(url);

      List<EnterpriseModel> enterprises = [];

      response.data['enterprises'].forEach((ent) {
        enterprises.add(EnterpriseModel.fromJson((ent)));
      });

      return ReponseData(success: true, data: enterprises);
    } on DioError catch (e) {
      print('error: ${e.toString()}');

      if (e.type == DioErrorType.RESPONSE) {
        if (e.response.statusCode == 401)
          return ReponseData(code: 1, message: 'Credenciais incorretas');
        else
          return ReponseData(
              code: 2,
              message: 'Ocorreu um erro interno no servidor, tente novamente');
      } else {
        if (e.type == DioErrorType.CONNECT_TIMEOUT)
          return ReponseData(
              code: 2, message: 'Tempo excedido da conexão, tente novamente');
        else
          return ReponseData(
              code: 2,
              message: 'Ocorreu um erro interno no servidor, tente novamente');
      }
    }
  }
}
