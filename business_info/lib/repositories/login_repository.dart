import 'package:business_info/models/user_model.dart';
import 'package:business_info/services/config_api.dart';
import 'package:business_info/services/response_data.dart';
import 'package:dio/dio.dart';

class LoginRepository {
  Future<ReponseData> login(UserModel userModel) async {
    var url = '$baseUrl/users/auth/sign_in';

    Dio dio = new Dio();

    dio.options.connectTimeout = 15000;

    try {
      Response response = await dio.post(url, data: {
        "email": userModel.email,
        "password": userModel.password,
      });

      return ReponseData(success: true, data: response.headers);
    } on DioError catch (e) {
      if (e.type == DioErrorType.RESPONSE) {
        if (e.response.statusCode == 401)
          return ReponseData(code: 1, message: 'Credenciais incorretas');
        else
          return ReponseData(
              code: 2,
              message: 'Ocorreu um erro interno no servidor, tente novamente');
      } else {
        if (e.type == DioErrorType.CONNECT_TIMEOUT)
          return ReponseData(
              code: 2, message: 'Tempo excedido da conexão, tente novamente');
        else
          return ReponseData(
              code: 2,
              message: 'Ocorreu um erro interno no servidor, tente novamente');
      }
    }
  }
}
