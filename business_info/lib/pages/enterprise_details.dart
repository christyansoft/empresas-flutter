import 'package:business_info/components/custom_app_bar.dart';
import 'package:business_info/models/enterprise_model.dart';
import 'package:flutter/material.dart';

class EnterpriseDetails extends StatefulWidget {
  final EnterpriseModel enterpriseModel;
  final Color color;
  EnterpriseDetails(this.enterpriseModel, this.color);

  @override
  _EnterpriseDetailsState createState() => _EnterpriseDetailsState();
}

class _EnterpriseDetailsState extends State<EnterpriseDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar('Detalhes da empresa', context),
      body: _body(),
    );
  }

  _body() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _header(),
            _elementEnterprise(
              'Ramo',
              widget.enterpriseModel.enterpriseType.enterpriseTypeName,
            ),
            _elementEnterprise(
              'Descrição',
              widget.enterpriseModel.description,
            ),
            _elementEnterprise(
              'Ação',
              widget.enterpriseModel.sharePrice
                  .toStringAsFixed(2)
                  .replaceAll('.', ','),
              icon: Icons.monetization_on_outlined,
            ),
            _elementEnterprise(
              'País',
              widget.enterpriseModel.country,
              icon: Icons.language,
            ),
            _elementEnterprise(
              'Cidade',
              widget.enterpriseModel.city,
              icon: Icons.location_city,
            ),
          ],
        ),
      ),
    );
  }

  _header() {
    return Container(
      height: 100,
      color: widget.color,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/images/ellipse_1.png'),
          SizedBox(
            height: 10,
          ),
          Center(
            child: Text(
              widget.enterpriseModel.enterpriseName,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  _elementEnterprise(String label, var content, {icon}) {
    return Container(
      margin: EdgeInsets.only(left: 16, top: 12, right: 16, bottom: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              if (icon != null)
                Icon(
                  icon,
                  color: Theme.of(context).primaryColor,
                  size: 20,
                ),
              if (icon != null)
                SizedBox(
                  width: 4,
                ),
              Text(
                label,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            content,
          ),
        ],
      ),
    );
  }
}
