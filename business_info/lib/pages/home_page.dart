import 'dart:math';

import 'package:business_info/components/loading_progress.dart';
import 'package:business_info/controllers/home_controller.dart';
import 'package:business_info/models/enterprise_model.dart';
import 'package:business_info/pages/enterprise_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeController _controller;

  final _cSearch = TextEditingController();

  @override
  void initState() {
    _controller = HomeController(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _controller.scaffoldKey,
      body: _body(),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: 220,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                gradient: SweepGradient(center: Alignment.topRight, colors: [
                  Colors.purple,
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColor,
                ])),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  alignment: Alignment.bottomLeft,
                  child: Image.asset('assets/images/logo_home.png'),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Image.asset('assets/images/logo_home-2.png'),
                ),
                Container(
                  alignment: Alignment.bottomRight,
                  child: Image.asset('assets/images/logo_home-1.png'),
                ),
                Container(
                  alignment: Alignment.topRight,
                  child: Image.asset('assets/images/logo_home-3.png'),
                ),
                Positioned(
                  bottom: -22.5,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    decoration: BoxDecoration(
                        color: Color(0xfff5f5f5),
                        borderRadius: BorderRadius.all(Radius.circular(4))),
                    height: 45,
                    child: TextFormField(
                      controller: _cSearch,
                      textInputAction: TextInputAction.search,
                      onFieldSubmitted: (String value) =>
                          _controller.onClickSearch(value),
                      style: TextStyle(color: Color(0xff666666)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.search,
                          color: Color(0xff666666),
                        ),
                        hintText: 'Pesquise por empresa',
                        hintStyle: TextStyle(color: Color(0xff666666)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          if (_cSearch.text.isNotEmpty) _listView()
        ],
      ),
    );
  }

  _listView() {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 32),
        child: Observer(builder: (_) {
          if (_controller.loadingSearch)
            return Container(
              margin: EdgeInsets.only(top: 64),
              child: loadingProgress(),
            );
          else if (_controller.enterprises.isEmpty)
            return _emptySearch();
          else {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 16),
                    child: Text(
                      '${_controller.enterprises.length} resultados encontrados',
                      style: TextStyle(color: Color(0xff666666), fontSize: 14),
                    ),
                  ),
                  Column(
                    children: _controller.enterprises
                        .map<Widget>((e) => _cardEnterprise(e))
                        .toList(),
                  ),
                ],
              ),
            );
          }
        }),
      ),
    );
  }

  _cardEnterprise(EnterpriseModel e) {
    Color color = Color(Random().nextInt(0xff79BBCA)).withAlpha(110);

    return Container(
      height: 100,
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Hero(
        tag: e.id,
        child: Card(
          color: color,
          child: ListTile(
            title: Text(
              e.enterpriseName.toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
            leading: Icon(
              Icons.store_mall_directory_outlined,
              color: Colors.white,
            ),
            trailing: Icon(Icons.keyboard_arrow_right_rounded),
            onTap: () => _onClickDetailsPage(e, color),
          ),
        ),
      ),
    );
  }

  _emptySearch() {
    return Container(
      margin: EdgeInsets.only(top: 64),
      child: Center(
        child: Text(
          'Nenhum resultado encontrado',
          style: TextStyle(fontSize: 18, color: Color(0xff666666)),
        ),
      ),
    );
  }

  _onClickDetailsPage(e, c) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => EnterpriseDetails(e, c)));

  @override
  void dispose() {
    _cSearch.dispose();
    super.dispose();
  }
}
