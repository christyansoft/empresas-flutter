import 'package:business_info/components/loading_progress.dart';
import 'package:business_info/controllers/login_controller.dart';
import 'package:business_info/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class SigninPage extends StatefulWidget {
  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  final _formKey = GlobalKey<FormState>();
  final _fPassword = FocusNode();

  LoginController _controller;
  UserModel userModel = UserModel();

  @override
  void initState() {
    _controller = LoginController(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _controller.scaffoldKey,
      body: _body(),
    );
  }

  _body() {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 300,
              margin: EdgeInsets.only(bottom: 32),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(400, 100),
                    bottomRight: Radius.elliptical(400, 100),
                  ),
                  gradient:
                      LinearGradient(begin: Alignment.bottomLeft, colors: [
                    Color(0xffC4C4C4),
                    Theme.of(context).primaryColor,
                    Theme.of(context).primaryColor,
                    Theme.of(context).primaryColor,
                    Theme.of(context).primaryColor,
                  ])),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Image.asset('assets/images/icon_ioasys.png'),
                    margin: EdgeInsets.only(bottom: 16),
                  ),
                  Center(
                    child: Text(
                      'Seja bem vindo ao empresas!',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      'Email',
                      style: TextStyle(color: Color(0xff666666)),
                    ),
                    margin: EdgeInsets.only(bottom: 6),
                  ),
                  Observer(builder: (_) {
                    return TextFormField(
                      onSaved: (String value) => userModel.email = value,
                      decoration: InputDecoration(
                          filled: true, errorText: _controller.errorField),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      onFieldSubmitted: (value) =>
                          FocusScope.of(context).requestFocus(_fPassword),
                      validator: (String text) =>
                          text.trim().isEmpty || !text.contains('@')
                              ? 'Informe um e-mail válido'
                              : null,
                    );
                  }),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      'Senha',
                      style: TextStyle(color: Color(0xff666666)),
                    ),
                    margin: EdgeInsets.only(bottom: 6),
                  ),
                  Observer(builder: (_) {
                    return TextFormField(
                      focusNode: _fPassword,
                      onSaved: (String value) => userModel.password = value,
                      obscureText: _controller.obscureText,
                      validator: (String text) =>
                          text.isEmpty ? 'Informe a senha' : null,
                      decoration: InputDecoration(
                          filled: true,
                          errorText: _controller.errorField,
                          suffixIcon: IconButton(
                            icon: Icon(_controller.obscureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                            splashRadius: 20,
                            onPressed: _controller.changeObscureText,
                          )),
                      textInputAction: TextInputAction.done,
                    );
                  }),
                ],
              ),
            ),
            SizedBox(
              height: 32,
            ),
            Observer(builder: (_) {
              if (_controller.loadingProgress) return loadingProgress();

              return Container(
                margin: EdgeInsets.only(left: 32, right: 32),
                width: MediaQuery.of(context).size.width,
                height: 45,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'ENTRAR',
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  onPressed: _onClickSignin,
                ),
              );
            })
          ],
        ),
      ),
    );
  }

  _onClickSignin() {
    if (_formKey.currentState.validate()) {
      _controller.store.changeError(null);
      _formKey.currentState.save();

      _controller.onClickLogin(userModel);
    }
  }
}
