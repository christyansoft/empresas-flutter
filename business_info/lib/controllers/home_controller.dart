import 'package:business_info/models/enterprise_model.dart';
import 'package:business_info/repositories/home_repository.dart';
import 'package:business_info/stores/home_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeController {
  BuildContext context;
  GlobalKey<ScaffoldState> scaffoldKey;

  HomeController(context) {
    this.context = context;
    scaffoldKey = GlobalKey<ScaffoldState>();
  }

  final repository = HomeRepository();
  final store = HomeStore();

  List<EnterpriseModel> get enterprises => store.enterprises;
  bool get loadingSearch => store.loadingSearch;

  onClickSearch(String query) async {
    store.changeLoadingSearch();

    repository.searchByName(query).then((responseData) async {
      if (responseData.success) {
        store.loadEnterprises(responseData.data);
      } else
        _showSnackbar(responseData.message);

      store.changeLoadingSearch();
    });
  }

  _showSnackbar(String text) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(text),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
    ));
  }
}
