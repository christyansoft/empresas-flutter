import 'package:business_info/models/credential_model.dart';
import 'package:business_info/models/user_model.dart';
import 'package:business_info/pages/home_page.dart';
import 'package:business_info/repositories/login_repository.dart';
import 'package:business_info/services/login_preferences.dart';
import 'package:business_info/stores/login_store.dart';
import 'package:flutter/material.dart';

class LoginController {
  BuildContext context;
  GlobalKey<ScaffoldState> scaffoldKey;
  LoginPreferences preferences;

  LoginController(context) {
    this.context = context;
    scaffoldKey = GlobalKey<ScaffoldState>();
    preferences = LoginPreferences();
  }

  final store = LoginStore();
  final repository = LoginRepository();

  get obscureText => store.obscureText;
  changeObscureText() => store.changeObscureText();

  get errorField => store.error;

  get loadingProgress => store.loadingSign;

  onClickLogin(UserModel userModel) async {
    store.changeLoadingSign();
    repository.login(userModel).then((responseData) async {
      store.changeLoadingSign();

      if (responseData.success) {
        await _saveCredentials(responseData.data);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        if (responseData.code == 1)
          store.changeError(responseData.message);
        else
          _showSnackbar(responseData.message);
      }
    });
  }

  _saveCredentials(data) async {
    var credential = CredentialModel(
        accessToken: data.value('access-token'),
        client: data.value('client'),
        uid: data.value('uid'));

    await preferences.saveCredentials(credential);
  }

  _showSnackbar(String text) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(text),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
    ));
  }
}
