import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {
  @observable
  bool obscureText = true;

  @action
  changeObscureText() => obscureText = !obscureText;

  @observable
  var error;

  @action
  changeError(value) => error = value;

  @observable
  bool loadingSign = false;

  @action
  bool changeLoadingSign() => loadingSign = !loadingSign;
}
