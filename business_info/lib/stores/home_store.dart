import 'package:business_info/models/enterprise_model.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore = _HomeStore with _$HomeStore;

abstract class _HomeStore with Store {
  @observable
  List<EnterpriseModel> enterprises = <EnterpriseModel>[];

  @action
  void loadEnterprises(List<EnterpriseModel> ent) =>
      enterprises = List.from(ent);

  @action
  void cleanEnterprises() => enterprises.clear();

  @observable
  bool loadingSearch = false;

  @action
  changeLoadingSearch() => loadingSearch = !loadingSearch;
}
